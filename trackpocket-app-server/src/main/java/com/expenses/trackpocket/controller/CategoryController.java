package com.expenses.trackpocket.controller;

import com.expenses.trackpocket.model.Category;
import com.expenses.trackpocket.security.CurrentUser;
import com.expenses.trackpocket.security.UserPrincipal;
import com.expenses.trackpocket.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/category")
public class CategoryController {

    @Autowired
    CategoryService categoryService;

    private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);

    public void removeCategory() {

    }

    @GetMapping
    public List<Category> getCategory(@CurrentUser UserPrincipal currentUser){
        List<Category> categoryList = new ArrayList<>();

        categoryList.addAll(categoryService.getCategoryByUser(currentUser.getUsername()));
        categoryList.addAll(categoryService.getDefaultCategoryList());

        logger.info(categoryList.toString());
        return categoryList;
    }

}
