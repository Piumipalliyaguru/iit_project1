package com.expenses.trackpocket.controller;

import com.expenses.trackpocket.model.Transaction;
import com.expenses.trackpocket.payload.ApiResponse;
import com.expenses.trackpocket.payload.PagedResponse;
import com.expenses.trackpocket.payload.TransactionRequest;
import com.expenses.trackpocket.payload.TransactionResponse;
import com.expenses.trackpocket.security.CurrentUser;
import com.expenses.trackpocket.security.UserPrincipal;
import com.expenses.trackpocket.service.TransactionService;
import com.expenses.trackpocket.util.AppConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @GetMapping
    public PagedResponse<TransactionResponse> getAllTransactions(@CurrentUser UserPrincipal currentUser,
                                                                 @RequestParam(value="page", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) int page,
                                                                 @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_PAGE_SIZE) int size) {
        return transactionService.getAllTransactions(currentUser, page, size);
    }

    @PostMapping("/add")
//    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<?> addTransaction(@CurrentUser UserPrincipal currentUser, @RequestBody TransactionRequest transactionRequest) {
         Transaction transaction = transactionService.addTransaction(transactionRequest, currentUser);


        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{transactionId}")
                .buildAndExpand(transaction.getId()).toUri();

        return ResponseEntity.created(location)
                .body(new ApiResponse(true, "Transaction added Successfully"));
    }
}
