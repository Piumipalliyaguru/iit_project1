package com.expenses.trackpocket.payload;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Getter @Setter @NoArgsConstructor
public class TransactionResponse {

    private int id;
    private String transactionType;
    private String paymentAccount;
    private String category;
    private UserSummary user;
    private String description;
    private double amount;
    private Instant transActionDateTime;


}
