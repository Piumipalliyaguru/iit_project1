package com.expenses.trackpocket.payload;

import com.expenses.trackpocket.model.Category;
import com.expenses.trackpocket.model.PaymentAccount;
import com.expenses.trackpocket.model.TransactionType;
import com.expenses.trackpocket.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.Instant;
import java.util.List;

@Getter @Setter @NoArgsConstructor
public class TransactionRequest {

    private String transactionType;

    private String paymentType;

    private String categoryType;

    private User user;

    private String note;

    private double amount;

    private Instant date;
}
