package com.expenses.trackpocket.payload;

import com.expenses.trackpocket.model.AddBy;
import com.expenses.trackpocket.model.TransactionType;
import com.expenses.trackpocket.repository.TransactionTypeRepository;

public class CategoryResponse {
    private int id;
    private TransactionTypeRepository transactionType;
    private String categoryName;
    private AddBy addBy;

    public CategoryResponse(int id, String categoryName, TransactionType transactionType, AddBy addBy) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TransactionTypeRepository getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionTypeRepository transactionType) {
        this.transactionType = transactionType;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public AddBy getAddBy() {
        return addBy;
    }

    public void setAddBy(AddBy addBy) {
        this.addBy = addBy;
    }
}
