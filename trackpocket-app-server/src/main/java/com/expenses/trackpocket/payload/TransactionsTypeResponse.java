package com.expenses.trackpocket.payload;

import com.expenses.trackpocket.model.TransActionName;

public class TransactionsTypeResponse {

    private int id;
    private TransActionName name;

    public TransactionsTypeResponse(int id, TransActionName name) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TransActionName getName() {
        return name;
    }

    public void setName(TransActionName name) {
        this.name = name;
    }
}
