package com.expenses.trackpocket.payload;

import com.expenses.trackpocket.model.PaymentAccountName;

public class PaymentTypeResponse {

    private int id;
    private PaymentAccountName paymentType;

    public PaymentTypeResponse(int id, PaymentAccountName paymentType) {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PaymentAccountName getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentAccountName paymentType) {
        this.paymentType = paymentType;
    }
}
