package com.expenses.trackpocket.model;

public enum StatusType {
    ACTIVE,
    HOLD,
    DE_ACTIVE,
    DELETED
}
