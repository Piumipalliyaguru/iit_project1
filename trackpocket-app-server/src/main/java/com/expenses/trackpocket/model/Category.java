package com.expenses.trackpocket.model;

import com.expenses.trackpocket.model.audit.UserDateAudit;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "category")
@Getter
@Setter
@NoArgsConstructor
public class Category extends UserDateAudit implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "transactionType_id", nullable = false)
    private TransactionType transactionType;

    @Column(name = "category_name")
    private String categoryName;

    @Enumerated(EnumType.STRING)
    @Column(name = "add_by")
    private AddBy addBy;

    public Category(TransactionType transactionType, String categoryName, AddBy addBy) {
        this.transactionType = transactionType;
        this.categoryName = categoryName;
        this.addBy = addBy;
    }
}
