package com.expenses.trackpocket.model;

public enum TransActionName {
    Income,
    Expenses
}
