package com.expenses.trackpocket.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
