package com.expenses.trackpocket.model;

public enum PaymentAccountName {
    Cash,
    Credit,
    Debit
}
