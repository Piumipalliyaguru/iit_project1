package com.expenses.trackpocket.model;

public enum AddBy {
    USER,
    SYSTEM
}
