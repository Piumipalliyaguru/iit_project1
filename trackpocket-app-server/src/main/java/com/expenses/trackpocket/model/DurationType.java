package com.expenses.trackpocket.model;

public enum  DurationType {
    WEEK,
    MONTH
}
