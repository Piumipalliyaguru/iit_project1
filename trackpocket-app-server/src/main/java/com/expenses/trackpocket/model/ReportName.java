package com.expenses.trackpocket.model;

public enum ReportName {
    WEEKLY,
    MONTHLY,
    CATEGORY_BASE,
    USER,
    TRANSACTION_TYPE_BASE,
    PAYMENT_ACCOUNT_BASE
}
