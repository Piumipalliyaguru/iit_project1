package com.expenses.trackpocket.model;

import javax.persistence.*;

@Entity
@Table(name ="transaction_type")
public class TransactionType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Enumerated(EnumType.STRING)
    @Column(name = "name")
    private TransActionName name;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public TransActionName getName() {
        return name;
    }

    public void setName(TransActionName name) {
        this.name = name;
    }
}
