package com.expenses.trackpocket.util;

import com.expenses.trackpocket.model.*;
import com.expenses.trackpocket.payload.*;

public class ModelMapper {

    public static TransactionResponse mapTransactionToTransactionResponse(Transaction transaction, TransactionType transactionType, PaymentAccount paymentAccount, Category category, User creator) {
        TransactionResponse transactionResponse = new TransactionResponse();

        UserSummary creatorSummary = new UserSummary(creator.getId(), creator.getUsername(), creator.getName());

        String transType = transactionType.getName().toString();
        String payType = paymentAccount.getPaymentType().toString();
        String catType = category.getCategoryName().toString();

        transactionResponse.setId(transaction.getId());
        transactionResponse.setTransactionType(transType);
        transactionResponse.setCategory(catType);
        transactionResponse.setPaymentAccount(payType);
        transactionResponse.setAmount(transaction.getAmount());
        transactionResponse.setDescription(transaction.getDescription());
        transactionResponse.setUser(creatorSummary);
        transactionResponse.setTransActionDateTime(transaction.getCreatedAt());

        return transactionResponse;
    }

}
