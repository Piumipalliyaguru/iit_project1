package com.expenses.trackpocket.repository;

import com.expenses.trackpocket.model.PaymentAccount;
import com.expenses.trackpocket.model.PaymentAccountName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PaymentAccountRepository extends JpaRepository<PaymentAccount, Integer> {

    PaymentAccount findByPaymentType(PaymentAccountName paymentName);
}
