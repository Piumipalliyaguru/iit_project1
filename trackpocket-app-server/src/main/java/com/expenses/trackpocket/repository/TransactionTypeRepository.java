package com.expenses.trackpocket.repository;

import com.expenses.trackpocket.model.TransActionName;
import com.expenses.trackpocket.model.TransactionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TransactionTypeRepository extends JpaRepository<TransactionType, Integer> {

//    @Query("SELECT t FROM TransactionType where t.name =:name")
//    TransactionType findByName(@Param("name") TransActionName transActionName);

    TransactionType findByName(TransActionName transActionName);

}
