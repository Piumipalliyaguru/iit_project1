package com.expenses.trackpocket.service;

import com.expenses.trackpocket.exception.AppException;
import com.expenses.trackpocket.exception.ResourceNotFoundException;
import com.expenses.trackpocket.model.*;
import com.expenses.trackpocket.payload.PagedResponse;
import com.expenses.trackpocket.payload.TransactionRequest;
import com.expenses.trackpocket.payload.TransactionResponse;
import com.expenses.trackpocket.repository.*;
import com.expenses.trackpocket.security.UserPrincipal;
import com.expenses.trackpocket.util.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TransactionTypeRepository transactionTypeRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    PaymentAccountRepository paymentAccountRepository;


    private static final Logger logger = LoggerFactory.getLogger(TransactionService.class);

    public PagedResponse<TransactionResponse> getAllTransactions(UserPrincipal currentUser, int page, int size) {

        Pageable pageable = PageRequest.of(page, size, Sort.Direction.DESC, "createdAt");
        Page<Transaction> transactions = transactionRepository.findByCreatedBy(currentUser.getId(),pageable);

        if(transactions.getNumberOfElements() == 0) {
            return new PagedResponse<>(Collections.emptyList(), transactions.getNumber(),
                    transactions.getSize(), transactions.getTotalElements(), transactions.getTotalPages(), transactions.isLast());
        }

        User user = userRepository.findByUsername(currentUser.getUsername()).orElseThrow(() -> new AppException("User not found"));

        List<TransactionResponse> transactionResponses = transactions.map(transaction -> {
            return ModelMapper.mapTransactionToTransactionResponse
                    (transaction, transaction.getTransactionType(), transaction.getPaymentAccount(), transaction.getCategory(), user );
        }).getContent();

        return new PagedResponse<>(transactionResponses, transactions.getNumber(),
                transactions.getSize(), transactions.getTotalElements(),
                transactions.getTotalPages(), transactions.isLast());
    }

    public Transaction addTransaction(TransactionRequest transactionRequest, UserPrincipal currentUser) {
        Transaction transaction = new Transaction();

        TransActionName transActionName = convertToTransName(transactionRequest);
        PaymentAccountName paymentAccountName = convertToPaymentName(transactionRequest);

        transaction.setUser(getUser(currentUser));
        transaction.setTransactionType(getTransactionType(transActionName));
        transaction.setPaymentAccount(getPaymentType(paymentAccountName));
        transaction.setCategory(getCategory(transactionRequest));
        transaction.setDescription(transactionRequest.getNote());
        transaction.setAmount(Double.valueOf(transactionRequest.getAmount()));
        transaction.setTransActionDateTime(transactionRequest.getDate());
        return transactionRepository.save(transaction);
    }

    public User getUser(UserPrincipal currentUser) {
        User user = userRepository.findByUsername(currentUser.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", currentUser.getUsername()));
        return user;
    }

    public Category getCategory(TransactionRequest transactionRequest) {
        Category category = categoryRepository.findByCategoryName(transactionRequest.getCategoryType())
                .orElseThrow(() ->new ResourceNotFoundException("Category", "categoryName", transactionRequest.getCategoryType()));
        return category;
    }

    public TransActionName convertToTransName(TransactionRequest transactionRequest) {
        TransActionName transActionName = TransActionName.valueOf(transactionRequest.getTransactionType());
        return transActionName;
    }

    public TransactionType getTransactionType(TransActionName transActionName) {
            return transactionTypeRepository.findByName(transActionName);
    }

    public PaymentAccountName convertToPaymentName(TransactionRequest transactionRequest){
        PaymentAccountName paymentAccountName = PaymentAccountName.valueOf(transactionRequest.getPaymentType());
        return paymentAccountName;
    }

    public PaymentAccount getPaymentType(PaymentAccountName paymentAccountName) {
        return paymentAccountRepository.findByPaymentType(paymentAccountName);
    }

//    public String paymentTypeName(Transaction transaction) {
//        return paymentAccountRepository.findById(transaction.getPaymentAccount()).toString();
//    }
//
//    public String transactionTypeName(Transaction transaction) {
//        return transactionTypeRepository.f
//    }

}
