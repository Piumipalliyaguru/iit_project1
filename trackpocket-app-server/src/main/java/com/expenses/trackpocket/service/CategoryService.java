package com.expenses.trackpocket.service;

import com.expenses.trackpocket.exception.AppException;
import com.expenses.trackpocket.exception.ResourceNotFoundException;
import com.expenses.trackpocket.model.*;
import com.expenses.trackpocket.payload.CategoryRequest;
import com.expenses.trackpocket.repository.CategoryRepository;
import com.expenses.trackpocket.repository.TransactionTypeRepository;
import com.expenses.trackpocket.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CategoryRepository categoryRepository;

    @Autowired
    TransactionTypeRepository transactionTypeRepository;

    private static final Logger logger = LoggerFactory.getLogger(CategoryService.class);

    public List<Category> getCategoryByUser(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));

        List<Category> userAddedCategoryList = categoryRepository.findByCreatedByAndAddBy(user.getId(), AddBy.USER);

        return userAddedCategoryList;
    }

    public List<Category> getDefaultCategoryList() {
        List<Category> systemAddedCategoryList = categoryRepository.findByAddBy(AddBy.SYSTEM);

        return systemAddedCategoryList;
    }
}
